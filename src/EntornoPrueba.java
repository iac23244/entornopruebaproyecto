import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.ColorDescriptor;
import org.eclipse.swt.graphics.RGB;

public class EntornoPrueba {

	protected Shell shell;
	private Text txtQueEsLo;
	private LocalResourceManager localResourceManager;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			EntornoPrueba window = new EntornoPrueba();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		createResourceManager();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblHola = new Label(shell, SWT.NONE);
		lblHola.setForeground(localResourceManager.create(ColorDescriptor.createFrom(new RGB(255, 0, 0))));
		lblHola.setBounds(10, 10, 53, 30);
		lblHola.setText("Nombre");
		
		txtQueEsLo = new Text(shell, SWT.BORDER);
		txtQueEsLo.setText("Que es lo que pasa");
		txtQueEsLo.setBounds(134, 38, 76, 21);

	}
	private void createResourceManager() {
		localResourceManager = new LocalResourceManager(JFaceResources.getResources(),shell);
	}
}
